using System;
using Finit;

namespace Finit.Robot
{
    public class Manipulator<T>
    {
        public delegate void ManipulatorFunc(T arg);
        public ManipulatorFunc Operate;
        
        public Manipulator(ManipulatorFunc manipulatorFunc)
        {
            Operate = manipulatorFunc;
        }
    }
}
