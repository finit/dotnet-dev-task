using System;
using Finit.Robot;

namespace Finit.Robot
{
    public interface IRobot
    {
        void DoTurn(Hardware hardware);
        void Reboot();
    }
}

