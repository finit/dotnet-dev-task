using System;
using Finit.Robot;
using System.Collections.Generic;

namespace Finit
{
    class MainClass
    {
        private static bool Verbose = false;

        private static void Summary(MissionResult result)
        {
            Console.WriteLine(String.Format ("{0} ({1}/{2} codebreak attempts)", result.Outcome, result.CodebreakAttempts, result.CodebreakAttemptsMax));
        }

        private static void Report(string name, MissionResult result)
        {
            Console.WriteLine(String.Format("Mission ended.\r\n{0}", result));
            switch (result.Outcome) {
            case MissionOutcome.SavedTheWorld:
                Console.WriteLine(String.Format("Brave robot {0} saved the world, congratulations! :-)", name));
                break;
                
            case MissionOutcome.Exploded:
                Console.WriteLine(String.Format("Unfortunately, {0} could not prevent the explosion. The world as we know it ceased to exist :-(", name));
                break;
                
            case MissionOutcome.OutOfFuel:
            case MissionOutcome.OutOfTime:
                Console.WriteLine(String.Format("{0} could not make it to the bomb in time. Unable to do a thing she watched this world burn to ashes.", name));
                break;
            }
            
            Console.WriteLine();
        }

        public static void Main(string[] args)
        {
            Verbose = args.Length > 0 && args[0] == "-v";

            var robot = new Alice();
            string name = robot.GetType().Name;

            Dictionary<string,Mission> missions = new Dictionary<string,Mission>();

            var room = new Room(10, 10, 3);
            var smallRoom = new Room(1, 10, 2);
            var largeRoom = new Room(50, 50, 7);

            missions.Add("trivial", new Mission(robot, room, 1000, 1000.0, 1000));
            missions.Add("simple", new Mission(robot, room));
            missions.Add("lucky", new Mission(robot, room, 1000, 1000.0, 3));
            missions.Add("hardcore", new Mission(robot, room, 1000, 200.0, 5));
            missions.Add("tricky", new Mission(robot, smallRoom, 1000, 200.0));
            missions.Add("big", new Mission(robot, largeRoom, 100000, 5000.0));

            foreach (KeyValuePair<string,Mission> i in missions) {
                if (Verbose) {
                    Console.WriteLine(String.Format("Sergeant {0} is being deployed on a \"{1}\" mission...", name, i.Key));
                } else {
                    Console.Write(String.Format("{0,-8}\t", i.Key));
                }

                var result = i.Value.Run();
                room.Reset();
                smallRoom.Reset();
                largeRoom.Reset();

                if (Verbose) {
                    Report(name, result);
                } else {
                    Summary(result);
                }
            }
        }
    }
}
